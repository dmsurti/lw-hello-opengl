This a Lispworks FFI based binding to modern programmable pipeline OpenGL. 

This is only a Hello World level prototype implementation to connect to modern
Opengl using LW FLI, glew, glfw3.

This is tested on Mac OS X 10.10 with LW 6.1, 32 bit.

Do the following on the REPL after cloing this repo:
* (load "~/path to init.lisp")

If loading any of libGL, libGLU, libglfw3, libglew dynamic libraries fail,
check the paths to these libraries in various `load-*` functions.

The `init.lisp` is really a port of anton's `first_test` in his awesome Opengl
tutorials.

Future Steps
---

* Follow along and port all the examples from anton's to work with LW and glfw.

* This must be refactored into the following:
    * lw-gl : A library for modern Opengl bindings using LW FLI
    * lw-glew : A library for glew bindings
    * lw-glfw3: A library for glfw3 bindings
    * lw-antons-glfw: Port anton's examples using the above 3
    * lw-gl-capi :  A library that implements a CAPI opengl output pane. See
      Camille's lw-opengl and LW Opengl old pipeline port which has an opengl
      output pane implementation. This will use lw-gl.
    * lw-antons-capi : Port of anton's examples using lw-gl-capi.
