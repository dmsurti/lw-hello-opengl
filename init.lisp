;;; Load the OpenGL dylibs
(defun load-opengl ()
  (let ((root "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk/System/Library/Frameworks/OpenGL.framework"))
    (fli:register-module (merge-pathnames "libGL.dylib" root))  
    (fli:register-module (merge-pathnames "libGLU.dylib" root))))

;;; ----------
;;; FLI for GL
;;; ----------

(defconstant GL-TRUE 1)
(defconstant GL-FALSE 0)

(defconstant GL-RENDERER #x1F01)
(defconstant GL-VERSION  #x1F02)

(fli:define-foreign-function
        (gl-get-string "glGetString" :source)
        ((name :int))
        :result-type (:reference-pass :ef-mb-string)
        :language :ansi-c)

;;; ------------
;;; FLI for GLEW
;;; ------------

(defun load-glew ()
  (fli:register-module "/usr/local/lib/libGLEW.dylib"))

(fli:define-foreign-function
        (glew-init "glewInit" :source)
        ()
        :result-type :void
        :language :ansi-c)

;;; ------------
;;; FLI for GLFW
;;; ------------

(defun load-glfw ()
  (fli:register-module "/usr/local/lib/libglfw3.dylib"))

(defconstant GLFW-CONTEXT-VERSION-MAJOR  #x00022002)
(defconstant GLFW-CONTEXT-VERSION-MINOR  #x00022003)
(defconstant GLFW-OPENGL-FORWARD-COMPAT  #x00022006)
(defconstant GLFW-OPENGL-PROFILE         #x00022008)
(defconstant GLFW-OPENGL-CORE-PROFILE    #x00032001)

(fli:define-foreign-function 
        (glfw-init "glfwInit" :source)
        ()
    :result-type :int
    :language :ansi-c)

(fli:define-foreign-function
        (glfw-window-hint "glfwWindowHint" :source)
        ((target :int)
         (hint :int))
    :result-type :int
    :language :ansi-c)

(fli:define-foreign-function
        (glfw-create-window "glfwCreateWindow" :source)
        ((width :int)
         (height :int)
         (title (:reference-pass :ef-mb-string))
         (monitor :pointer)
         (window :pointer))
        :result-type :pointer
        :language :ansi-c)

(fli:define-foreign-function
        (glfw-make-context-current "glfwMakeContextCurrent" :source)
        ((window :pointer))
        :result-type :void
        :language :ansi-c)

(fli:define-foreign-function
        (glfw-terminate "glfwTerminate" :source)
        ()
    :result-type :void
    :language :ansi-c)

(defun setup-opengl-hints ()
  (glfw-window-hint GLFW-CONTEXT-VERSION-MAJOR 4)
  (glfw-window-hint GLFW-CONTEXT-VERSION-MINOR 1)
  (glfw-window-hint GLFW-OPENGL-FORWARD-COMPAT GL-TRUE)
  (glfw-window-hint GLFW-OPENGL-PROFILE GLFW-OPENGL-CORE-PROFILE))

;;; --------
;;; Start up
;;; --------

(defun main ()
  (load-opengl)
  (load-glew)
  (load-glfw)
  (let ((init (glfw-init)))
    (if (zerop init)
      (format t " Could not start GLFW3 ~%")
      (progn
        (setup-opengl-hints)
        (let ((win-ptr (glfw-create-window 640
                                           480
                                           "Hello Triangle"
                                           fli:*null-pointer*
                                           fli:*null-pointer*)))
          (if (fli:null-pointer-p win-ptr)
            (format t " Could not open window with GLFW3 ~%"))
            (progn
              (format t " Window created succesfully ~%")
              (glfw-make-context-current win-ptr)
              (glew-init)
              (let ((renderer (gl-get-string GL-RENDERER))
                    (version (gl-get-string GL-VERSION)))
                (format t "Renderer: ~A ~%" renderer)
                (format t "OpenGL version supported ~A ~%" version))
              (glfw-terminate)))))))
